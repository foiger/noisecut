import tensorflow as tf
from tensorflow import keras
import numpy as np

autoencoder = tf.keras.models.load_model('model.h5')

# Check its architecture
autoencoder.summary()

from tqdm import tqdm
import glob
import os

from scipy.io import wavfile
import time
import math
from PIL import Image
import joblib

FFT_LENGTH = 512
WINDOW_LENGTH = 512
WINDOW_STEP = int(WINDOW_LENGTH / 2)
magnitudeMin = float("inf")
magnitudeMax = float("-inf")
phaseMin = float("inf")
phaseMax = float("-inf")

def standardizeWav(filePath, fileName):
    path = os.path.join(filePath, fileName)
    rate, data = wavfile.read(path)

    if len(data.shape) >= 2 and data.size > 0:
        if data.shape[-1] > 1:
            data = data.mean(axis = -1)
        else:
            data = np.reshape(data, data.shape[:-1])
            
    current_size = np.shape(data)[0]
    print(current_size, current_size / float(rate))
    start = 0
    res = []
    while current_size // float(rate) > 0:
        print("val:", current_size / float(rate))
        res.append(data[start: start + 16000])
        print(len(res[-1]))
        start += 16000
        current_size -= 16000
        
    
    return res


def generateSpectrogramForWave(signal):
    start_time = time.time()
    global magnitudeMin
    global magnitudeMax
    global phaseMin
    global phaseMax
    buffer = np.zeros(int(signal.size + WINDOW_STEP - (signal.size % WINDOW_STEP)))
    buffer[0:len(signal)] = signal
    height = int(FFT_LENGTH / 2 + 1)
    width = int(len(buffer) / WINDOW_STEP - 1)
    magnitudePixels = np.zeros((height, width))
    phasePixels = np.zeros((height, width))
    
    for w in range(width):
        buff = np.zeros(FFT_LENGTH)
        stepBuff = buffer[w*WINDOW_STEP:w*WINDOW_STEP + WINDOW_LENGTH]
        stepBuff = stepBuff * np.hanning(WINDOW_LENGTH)
        buff[0:len(stepBuff)] = stepBuff
        fft = np.fft.rfft(buff)
        for h in range(len(fft)):
            magnitude = math.sqrt(fft[h].real**2 + fft[h].imag**2)
            if magnitude > magnitudeMax:
                magnitudeMax = magnitude 
            if magnitude < magnitudeMin:
                magnitudeMin = magnitude
                
            phase = math.atan2(fft[h].imag, fft[h].real)
            if phase > phaseMax:
                phaseMax = phase
            if phase < phaseMin:
                phaseMin = phase
                
            magnitudePixels[height-h-1,w] = magnitude
            phasePixels[height-h-1,w] = phase
            
    rgbArray = generateLinearScale(magnitudePixels, phasePixels,
                                  magnitudeMin, magnitudeMax, phaseMin, phaseMax)
    print(rgbArray.shape)
    elapsed_time = time.time() - start_time
    print('%.2f' % elapsed_time, 's', sep='')
    img = Image.fromarray(rgbArray, 'RGB')
  #  img.show()
    return img

def generateLinearScale(magnitudePixels, phasePixels, 
                        magnitudeMin, magnitudeMax, phaseMin, phaseMax):
    height = magnitudePixels.shape[0]
    width = magnitudePixels.shape[1]
    magnitudeRange = magnitudeMax - magnitudeMin
    phaseRange = phaseMax - phaseMin
    rgbArray = np.zeros((height, width, 3), 'uint8')
    
    for w in range(width):
        for h in range(height):
            magnitudePixels[h,w] = (magnitudePixels[h,w] - magnitudeMin) / (magnitudeRange) * 255 * 2
            magnitudePixels[h,w] = amplifyMagnitudeByLog(magnitudePixels[h,w])
            phasePixels[h,w] = (phasePixels[h,w] - phaseMin) / (phaseRange) * 255
            red = 255 if magnitudePixels[h,w] > 255 else magnitudePixels[h,w]
            green = (magnitudePixels[h,w] - 255) if magnitudePixels[h,w] > 255 else 0
            blue = phasePixels[h,w]
            rgbArray[h,w,0] = int(red)
            rgbArray[h,w,1] = int(green)
            rgbArray[h,w,2] = int(blue)
    return rgbArray


def imgToArray(img):
    return np.asarray(img)
    
def amplifyMagnitudeByLog(d):
    return 188.301 * math.log10(d + 1)

def weakenAmplifiedMagnitude(d):
    return math.pow(10, d/188.301) - 1

def recoverLinearScale(rgbArray, magnitudeMin, magnitudeMax, 
                       phaseMin, phaseMax):
    width = rgbArray.shape[1]
    height = rgbArray.shape[0]
    print(phaseMax,phaseMin)
    magnitudeVals = rgbArray[:,:,0].astype(float) + rgbArray[:,:,1].astype(float)
    phaseVals = rgbArray[:,:,2].astype(float)
    phaseRange = phaseMax - phaseMin
    magnitudeRange = magnitudeMax - magnitudeMin
     
#     print(magnitudeVals)
#     print(phaseVals)
#     print(phaseRange)
#     print(magnitudeRange)
    
    for w in range(width):
        for h in range(height):
            phaseVals[h,w] = (phaseVals[h,w] / 255 * phaseRange) + phaseMin
            magnitudeVals[h,w] = weakenAmplifiedMagnitude(magnitudeVals[h,w])
            magnitudeVals[h,w] = (magnitudeVals[h,w] / (255*2) * magnitudeRange) + magnitudeMin
            
    return magnitudeVals, phaseVals


ROW = 257
COL = 62
INPUT_DIM = (ROW,COL,3)
INPUT_DIM[:2]

from IPython.display import Audio
rate = 16000

def recoverSignalFromSpectrogram(data):
  #  data = np.array( img, dtype='uint16' )
    width = data.shape[1]
    height = data.shape[0]

    magnitudeVals, phaseVals \
    = recoverLinearScale(data, magnitudeMin, magnitudeMax, phaseMin, phaseMax)
    
    recovered = np.zeros(WINDOW_LENGTH * width // 2 + WINDOW_STEP, dtype=np.int16)
    recovered = np.array(recovered,dtype=np.int16)
    
    for w in range(width):
        toInverse = np.zeros(height, dtype=np.complex_)
        for h in range(height):
            magnitude = magnitudeVals[height-h-1,w]
            phase = phaseVals[height-h-1,w]
            toInverse[h] = magnitude * math.cos(phase) + (1j * magnitude * math.sin(phase))
        signal = np.fft.irfft(toInverse)
        recovered[w*WINDOW_STEP:w*WINDOW_STEP + WINDOW_LENGTH] += signal[:WINDOW_LENGTH].astype(np.int16)
    
    return recovered

import scipy

signals = standardizeWav('archive\BWAVN\BWAVN', '19-227-0007.wav')
imgs = []
for signal in signals:
    img = generateSpectrogramForWave(signal)
    imgs.append(imgToArray(img))
    
#print(imgs)
#data = recoverSignalFromSpectrogram(a)
#data.shape
Audio(data = signal, rate = rate)

rate = 16000
#test_file = a
#print(test_file.shape)
super_data = []
for img_test in imgs:
    img_test = img_test/255
    img_test = img_test.reshape(-1, ROW,COL,3)
    decoded_imgs = autoencoder.predict(img_test) #predict
    decoded_imgs = decoded_imgs.reshape(ROW,COL,3)
    decoded_imgs = decoded_imgs*255
    decoded_imgs = decoded_imgs.astype(np.int16)
    data = recoverSignalFromSpectrogram(decoded_imgs)
    super_data.extend(data)
    
super_data = np.array(super_data)
scipy.io.wavfile.write("predict.wav", rate, super_data)

